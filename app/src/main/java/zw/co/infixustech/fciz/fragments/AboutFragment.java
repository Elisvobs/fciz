package zw.co.infixustech.fciz.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.card.MaterialCardView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zw.co.infixustech.fciz.ContactUsActivity;
import zw.co.infixustech.fciz.PlansActivity;
import zw.co.infixustech.fciz.R;

public class AboutFragment extends Fragment implements View.OnClickListener {

    public AboutFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View aboutView = inflater.inflate(R.layout.fragment_about, container, false);
        setRetainInstance(true);

        MaterialCardView plan = aboutView.findViewById(R.id.funeral_plan);
        MaterialCardView contact = aboutView.findViewById(R.id.contact);
        MaterialCardView benefits = aboutView.findViewById(R.id.benefits);
        MaterialCardView services = aboutView.findViewById(R.id.services);
        MaterialCardView about = aboutView.findViewById(R.id.about_us);

        plan.setOnClickListener(this);
        contact.setOnClickListener(this);
        benefits.setOnClickListener(this);
        services.setOnClickListener(this);
        about.setOnClickListener(this);

        return aboutView;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.funeral_plan:
                getContext().startActivity(new Intent(getActivity(), PlansActivity.class));
                break;
            case R.id.contact:
                getContext().startActivity(new Intent(getActivity(), ContactUsActivity.class));
                break;
            case R.id.benefits:
//                getContext().startActivity(new Intent(getActivity(), BenefitsActivity.class));
                break;
            case R.id.services:
//                getContext().startActivity(new Intent(getActivity(), ServicesActivity.class));
                break;
            case R.id.about_us:
//                getContext().startActivity(new Intent(getActivity(), AboutUsActivity.class));
                break;
        }
    }
}