package zw.co.infixustech.fciz;

import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity {
    TextInputEditText mobileText, passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(R.string.action_login);

        mobileText = findViewById(R.id.mobile);
        passwordText = findViewById(R.id.loginPassword);
    }

    public void resetPassword(View view){

    }

    public void initSignUp(View view){
        startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }

    public void logIn(View view){

    }
}