package zw.co.infixustech.fciz;

import android.content.Intent;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class PlansActivity extends AppCompatActivity {
    ImageSwitcher switcher;
    int [] plans = {R.drawable.silver_plan, R.drawable.gold_plan, R.drawable.platinum_plan};
    int currentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        setTitle(R.string.select_plan);

        switcher = findViewById(R.id.imageSwitcher);
        switcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView image = new ImageView(getApplicationContext());
                image.setScaleType(ImageView.ScaleType.FIT_CENTER);
                image.setLayoutParams(new ImageSwitcher.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
                return image;
            }
        });
    }

    public void onClick(View view){
        int id = view.getId();
        switch (id){
            case R.id.prev:
                if (currentIndex > 0){
                    currentIndex = currentIndex - 1;
                    switcher.setBackgroundResource(plans[currentIndex]);
                }
                break;
            case R.id.next:
                if (currentIndex < plans.length - 1){
                    currentIndex = currentIndex + 1;
                    switcher.setBackgroundResource(plans[currentIndex]);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}