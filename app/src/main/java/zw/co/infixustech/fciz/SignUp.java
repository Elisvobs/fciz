package zw.co.infixustech.fciz;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class SignUp extends AsyncTask<String, Void, String> {

    Context context;

    SignUp(Context context) {
        this.context = context;
    }


    @Override
    protected String doInBackground(String... params) {
        String reg_url = "";
        String method = params[0];

        if(method.equals("register")){
            String name = params[1];
            String phone = params[2];
            String password = params[3];

            try {
                URL url = new URL(reg_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);

                InputStream inputStream = connection.getInputStream();
                OutputStream outputStream = connection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String data = String.format("%s=%s&%s=%s&%s=%s",
                        URLEncoder.encode("name", "UTF-8"), URLEncoder.encode(name, "UTF-8"),
                        URLEncoder.encode("phone", "UTF-8"), URLEncoder.encode(phone, "UTF-8"),
                        URLEncoder.encode("password", "UTF-8"), URLEncoder.encode(password, "UTF-8"));

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                inputStream.close();
                outputStream.close();

                return "Registration successful";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(context, result,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}