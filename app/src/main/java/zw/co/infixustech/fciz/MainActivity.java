package zw.co.infixustech.fciz;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import zw.co.infixustech.fciz.fragments.AboutFragment;
import zw.co.infixustech.fciz.fragments.DependencyFragment;
import zw.co.infixustech.fciz.fragments.InfoFragment;
import zw.co.infixustech.fciz.fragments.PlansFragment;

public class MainActivity extends AppCompatActivity {
    public Fragment fragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.plan) {
                fragment = new PlansFragment();
                setTitle(R.string.select_plan);
            } else if (id == R.id.dependency) {
                fragment = new DependencyFragment();
                setTitle(R.string.add_dependency);
            } else if (id == R.id.info) {
                fragment = new InfoFragment();
                setTitle(R.string.edit_info);
            } else if (id == R.id.about) {
                fragment = new AboutFragment();
                setTitle(R.string.about_ficz);
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_container, fragment).addToBackStack(null).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_share:
                break;
            case R.id.action_logout:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}