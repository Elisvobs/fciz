package zw.co.infixustech.fciz;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView fb;
    TextView web, email, locale, call, chat, msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        fb = findViewById(R.id.fb);
        web = findViewById(R.id.web);
        email = findViewById(R.id.send_email);
        locale = findViewById(R.id.locale);
        call = findViewById(R.id.call);
        chat = findViewById(R.id.chat);
        msg = findViewById(R.id.sms);

        fb.setOnClickListener(this);
        web.setOnClickListener(this);
        email.setOnClickListener(this);
        locale.setOnClickListener(this);
        call.setOnClickListener(this);
        chat.setOnClickListener(this);
        msg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.web) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW);

        } else if (id == R.id.send_email) {
            String emailAddress = "fcizinfo@ficzfuneral.co.zw";
            Uri email = Uri.parse("email:" + emailAddress);
            Intent emailIntent = new Intent(Intent.ACTION_SEND, email);
            emailIntent.setAction(Intent.ACTION_SEND);
        } else if (id == R.id.locale) {

        } else if (id == R.id.call) {
            String number = "263 71 744 2799";
            Uri call = Uri.parse("cell:" + number);
            Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
            callIntent.setAction(Intent.ACTION_DIAL);
        } else if (id == R.id.chat) {

        } else if (id == R.id.sms) {

        } else if (id == R.id.fb) {

        }
    }
}