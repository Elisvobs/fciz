package zw.co.infixustech.fciz;

import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {
    TextInputEditText nameText, phoneText, passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setTitle(R.string.action_register);

        nameText = findViewById(R.id.name);
        phoneText = findViewById(R.id.phone);
        passwordText = findViewById(R.id.password);
    }

    public void initLogin(View view){
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void signUp(View view){
        CheckBox checkBox = findViewById(R.id.terms);
        if (checkBox.isChecked()) {
            signUpRequest();
        } else {
            Toast.makeText(getApplicationContext(), "Please Accept Terms & Conditions", Toast.LENGTH_SHORT).show();
        }
    }

    private void signUpRequest() {
        String name = nameText.getEditableText().toString();
        String phone = phoneText.getEditableText().toString();
        String password = passwordText.getEditableText().toString();
        String method = "method";

        SignUp signUp = new SignUp(this);
        signUp.execute(method, name, phone, password);
    }
}