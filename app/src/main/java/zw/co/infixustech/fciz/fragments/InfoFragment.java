package zw.co.infixustech.fciz.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import zw.co.infixustech.fciz.R;

public class InfoFragment extends Fragment {

    public InfoFragment() {
    }

    RadioButton radioButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View infoView = inflater.inflate(R.layout.fragment_info, container, false);
        setRetainInstance(true);

        final TextInputEditText nameField = infoView.findViewById(R.id.name);
        final TextInputEditText idField = infoView.findViewById(R.id.number);
        final TextInputEditText emailField = infoView.findViewById(R.id.email);
        final TextInputEditText addressField = infoView.findViewById(R.id.address);
        final TextView textView = infoView.findViewById(R.id.dob);
        RadioGroup radioGroup = infoView.findViewById(R.id.sex);
        Button save = infoView.findViewById(R.id.save);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // calender class's instance
                final Calendar calendar = Calendar.getInstance();
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                LocalDate localDate;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    localDate = LocalDate.of(2019, 1,1);
                                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                    textView.setText(dateTimeFormatter.format(localDate));
                                } else {
                                    // set day of month , month and year value in the edit text
                                    calendar.set(Calendar.YEAR, year);
                                    calendar.set(Calendar.MONTH, monthOfYear);
                                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    textView.setText(DateFormat.getDateInstance(DateFormat.MEDIUM).format(calendar.getTime()));
                                }
                            }
                        },
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                radioButton = radioGroup.findViewById(checkedId);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        final String name = nameField.getEditableText().toString();
                        final String idNumber = idField.getEditableText().toString();
                        final String email = emailField.getEditableText().toString();
                        final String address = addressField.getEditableText().toString();
                        final String gender = radioButton.getText().toString();
                        final String birthDate = textView.getText().toString();

                        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(gender) || TextUtils.isEmpty(birthDate) || TextUtils.isEmpty(idNumber) || TextUtils.isEmpty(email)|| TextUtils.isEmpty(address)){
                            Toast.makeText(getActivity(), "Something went wrong. Please fill all field", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getActivity(), "Information successfully added.", Toast.LENGTH_LONG).show();
                        }
            }
        });

        return infoView;
    }
}